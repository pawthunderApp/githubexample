package com.pawthunder.githubexample.binding

import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter

object BindingAdapters {

    /**
     * Item is visible or gone depends on boolean value
     * @param view which visibility will be set
     * @param show decides if view is visible
     */
    @JvmStatic
    @BindingAdapter("visibleGone")
    fun showHide(view: View, show: Boolean) {
        view.visibility = if (show) View.VISIBLE else View.GONE
    }

    /**
     * Format double to simple string format
     * @param textView view containing text
     * @param value formed to string
     */
    @JvmStatic
    @BindingAdapter("formatDouble")
    fun formatDouble(textView: TextView, value: Double?) {
        textView.text = String.format("%.2f", value)
    }
}