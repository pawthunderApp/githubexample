package com.pawthunder.githubexample.repository.inmemory

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.pawthunder.githubexample.vo.api.NetworkState
import retrofit2.Call
import retrofit2.Response
import java.io.IOException
import java.util.concurrent.Executor

abstract class PageKeyDataSource<ItemType, RequestType>(
    private val networkExecutor: Executor
) : PageKeyedDataSource<Int, ItemType>() {

    private var retry: (() -> Any)? = null

    val networkState = MutableLiveData<NetworkState>()
    val initialLoad = MutableLiveData<NetworkState>()

    fun retryAllFailed() {
        val prevRetry = retry
        retry = null
        networkExecutor.execute {
            prevRetry?.invoke()
        }
    }

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, ItemType>) {
        val request = createInitialRequest(params)

        networkState.postValue(NetworkState.LOADING)
        initialLoad.postValue(NetworkState.LOADING)

        try {
            val response = request.execute()
            val items = retrieveItems(response)
            // return -1 if there is no previous page
            retry = null
            networkState.postValue(NetworkState.LOADED)
            initialLoad.postValue(NetworkState.LOADED)
            callback.onResult(items, -1, 2)
        } catch (ex: IOException) {
            retry = {
                loadInitial(params, callback)
            }

            NetworkState.error(ex.message ?: UNKNOWN_ERROR).let {
                networkState.postValue(it)
                initialLoad.postValue(it)
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, ItemType>) {
        networkState.postValue(NetworkState.LOADING)
        createAfterRequest(params).enqueue(
            object : retrofit2.Callback<RequestType> {
                override fun onFailure(call: Call<RequestType>, throwable: Throwable) {
                    retry = {
                        loadAfter(params, callback)
                    }
                    networkState.postValue(NetworkState.error(throwable.message ?: UNKNOWN_ERROR))
                }

                override fun onResponse(call: Call<RequestType>, response: Response<RequestType>) {
                    if (response.isSuccessful) {
                        val items = retrieveItems(response)
                        retry = null
                        networkState.postValue(NetworkState.LOADED)
                        callback.onResult(items, params.key + 1)
                    } else {
                        retry = {
                            loadAfter(params, callback)
                        }
                        networkState.postValue(NetworkState.error("All items loaded"))
                    }
                }
            }
        )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, ItemType>) {
        // ignored, we only append to initial load
    }

    /**
     * Initial PagingRequest for initial data is created with [LoadInitialParams] for PagingRequest.
     * @param params data for initial load
     * @return initialized call to execute PagingRequest
     * @see Call
     */
    abstract fun createInitialRequest(params: LoadInitialParams<Int>): Call<RequestType>

    /**
     * Create PagingRequest for loading additional items to list
     * @param params for loading additional pages
     * @return call for additional pages
     * @see Call
     */
    abstract fun createAfterRequest(params: LoadParams<Int>): Call<RequestType>

    /**
     * Items are retrieved from response return by REST API
     * @param response with list of items
     * @return list of items
     */
    abstract fun retrieveItems(response: Response<RequestType>): List<ItemType>

    companion object {
        /**
         * Default page size if you don't want to set custom
         */
        const val DEFAULT_PAGE_SIZE: Int = 20

        const val UNKNOWN_ERROR: String = "Unknown error"
    }
}