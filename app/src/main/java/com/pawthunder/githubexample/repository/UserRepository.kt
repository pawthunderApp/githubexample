package com.pawthunder.githubexample.repository

import androidx.lifecycle.LiveData
import com.pawthunder.githubexample.api.GithubService
import com.pawthunder.githubexample.db.UserDao
import com.pawthunder.githubexample.util.AppExecutors
import com.pawthunder.githubexample.vo.api.Resource
import com.pawthunder.githubexample.vo.db.User
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(
    private val appExecutors: AppExecutors,
    private val userDao: UserDao,
    private val githubService: GithubService
) {

    fun loadUser(login: String): LiveData<Resource<User>> {
        return object : NetworkBoundResource<User, User>(appExecutors) {
            override fun saveCallResult(item: User) {
                userDao.insertItem(item)
            }

            override fun shouldFetch(data: User?) = data == null

            override fun loadFromDb() = userDao.findByLogin(login)

            override fun createCall() = githubService.getUser(login)
        }.asLiveData()
    }
}