package com.pawthunder.githubexample.repository.search

import com.pawthunder.githubexample.api.GithubService
import com.pawthunder.githubexample.repository.inmemory.PageKeyDataSource
import com.pawthunder.githubexample.vo.db.Repository
import com.pawthunder.githubexample.vo.response.RepositorySearchResponse
import retrofit2.Response
import java.util.*
import java.util.concurrent.Executor

class PageKeyNewsDataSource(
    private val githubService: GithubService,
    private val query: String,
    networkExecutor: Executor
) : PageKeyDataSource<Repository, RepositorySearchResponse>(networkExecutor) {

    override fun createInitialRequest(params: LoadInitialParams<Int>) = githubService.searchRepos(query, 1)

    override fun createAfterRequest(params: LoadParams<Int>) = githubService.searchRepos(query, params.key)

    override fun retrieveItems(response: Response<RepositorySearchResponse>) =
        response.body()?.items ?: Collections.emptyList()
}