package com.pawthunder.githubexample.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.toLiveData
import com.a01people.bitpolis.vo.api.Listing
import com.pawthunder.githubexample.api.GithubService
import com.pawthunder.githubexample.db.RepositoryDao
import com.pawthunder.githubexample.repository.inmemory.PageKeyDataSource
import com.pawthunder.githubexample.repository.search.SearchDataSourceFactory
import com.pawthunder.githubexample.util.AppExecutors
import com.pawthunder.githubexample.vo.api.NetworkState
import com.pawthunder.githubexample.vo.db.Repository
import com.pawthunder.githubexample.vo.response.RepositorySearchResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class RepoRepository @Inject constructor(
    private val appExecutors: AppExecutors,
    private val repositoryDao: RepositoryDao,
    private val githubService: GithubService
) {

    private fun refresh(query: String): LiveData<NetworkState> {
        val networkState = MutableLiveData<NetworkState>()
        networkState.value = NetworkState.LOADING
        githubService.searchRepos(query, 1).enqueue(
            object : Callback<RepositorySearchResponse?> {
                override fun onFailure(call: Call<RepositorySearchResponse?>, error: Throwable) {
                    networkState.value = NetworkState.error(error.message)
                }

                override fun onResponse(
                    call: Call<RepositorySearchResponse?>,
                    response: Response<RepositorySearchResponse?>
                ) {
                    appExecutors.diskIO().execute {
                        val repositories = response.body()?.items
                        if (repositories != null) repositoryDao.insertItems(repositories)
                    }

                    networkState.postValue(NetworkState.LOADED)
                }
            }
        )
        return networkState
    }

    /**
     * Pagination with search is initialized
     * @param query for search
     * @return [Listing] containing values needed for pagination management
     */
    fun searchRepositories(query: String): Listing<Repository> {
        val sourceFactory =
            SearchDataSourceFactory(query, githubService, appExecutors.networkIO())

        val livePagedList = sourceFactory.toLiveData(
            pageSize = PageKeyDataSource.DEFAULT_PAGE_SIZE,
            fetchExecutor = appExecutors.networkIO()
        )

        val refreshState = Transformations.switchMap(sourceFactory.sourceLiveData) {
            it.initialLoad
        }

        return Listing(
            livePagedList,
            Transformations.switchMap(sourceFactory.sourceLiveData) { it.networkState },
            refreshState,
            { sourceFactory.sourceLiveData.value?.invalidate() },
            { sourceFactory.sourceLiveData.value?.retryAllFailed() }
        )
    }
}