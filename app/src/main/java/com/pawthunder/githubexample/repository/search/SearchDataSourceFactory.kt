package com.pawthunder.githubexample.repository.search

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.pawthunder.githubexample.api.GithubService
import com.pawthunder.githubexample.vo.db.Repository
import java.util.concurrent.Executor

class SearchDataSourceFactory(
    private val query: String,
    private val githubService: GithubService,
    private val networkExecutor: Executor
) : DataSource.Factory<Int, Repository>() {

    val sourceLiveData = MutableLiveData<PageKeyNewsDataSource>()

    override fun create(): DataSource<Int, Repository> {
        val source = PageKeyNewsDataSource(githubService, query, networkExecutor)
        sourceLiveData.postValue(source)
        return source
    }


}