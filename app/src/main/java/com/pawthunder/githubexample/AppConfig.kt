package com.pawthunder.githubexample

/**
 * Constants for association with external services like admob or firebase
 * @author Peter Grajko
 */
object AppConfig {

    private const val HOST_DOMAIN: String = "api.github.com"

    const val HOST: String = "https://$HOST_DOMAIN/"
}