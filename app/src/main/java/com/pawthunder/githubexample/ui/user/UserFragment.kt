package com.pawthunder.githubexample.ui.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.pawthunder.githubexample.R
import com.pawthunder.githubexample.databinding.FragmentUserBinding
import com.pawthunder.githubexample.di.Injectable
import com.pawthunder.githubexample.util.autoCleared
import javax.inject.Inject

class UserFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var userModel: UserViewModel

    private var binding by autoCleared<FragmentUserBinding>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_user, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        userModel = ViewModelProviders.of(this, viewModelFactory).get(UserViewModel::class.java)
    }
}
