package com.pawthunder.githubexample.ui.user

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.pawthunder.githubexample.repository.UserRepository
import com.pawthunder.githubexample.util.AbsentLiveData
import com.pawthunder.githubexample.vo.api.Resource
import com.pawthunder.githubexample.vo.db.User
import javax.inject.Inject

class UserViewModel @Inject constructor(
    userRepository: UserRepository
) : ViewModel() {
    private val login = MutableLiveData<String>()

    val user: LiveData<Resource<User>> = Transformations.switchMap(login) { data ->
        if (data == null) {
            AbsentLiveData.create()
        } else {
            userRepository.loadUser(data)
        }
    }

    fun setLogin(newLogin: String?) {
        if (login.value != newLogin) {
            login.value = newLogin
        }
    }

    fun retry() {
        login.value = login.value
    }
}