/*
 * Copyright (c) 2018 Peter Grajko. All rights reserved.
 *
 * Developed by Peter Grajko on 11/9/18 11:37 AM.
 * Last modified 11/9/18 11:36 AM.
 */

package com.pawthunder.githubexample.ui.common

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

/**
 * Holder for data in adapter
 * @author Peter Grajko
 * @since 1.0
 * @property binding of container
 */
class DataBoundHolder<out T : ViewDataBinding> constructor(val binding: T) : RecyclerView.ViewHolder(binding.root)