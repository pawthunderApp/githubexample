package com.pawthunder.githubexample.ui.repository

import androidx.databinding.DataBindingComponent
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.pawthunder.githubexample.R
import com.pawthunder.githubexample.databinding.ItemRepositoryBinding
import com.pawthunder.githubexample.repository.inmemory.PageKeyDataSource
import com.pawthunder.githubexample.ui.common.BoundPagedList
import com.pawthunder.githubexample.ui.common.ItemClickCallback
import com.pawthunder.githubexample.util.AppExecutors
import com.pawthunder.githubexample.vo.db.Repository

/**
 * Adapter showing repositories
 * @author Peter Grajko
 * @since 1.0
 * @property itemClickCallback trigger event when item is clicked
 */
class RepositotyAdapter(
    retryCallback: () -> Unit,
    bindingComponent: DataBindingComponent,
    appExecutors: AppExecutors,
    private val itemClickCallback: ItemClickCallback
) : BoundPagedList<Repository>(
    retryCallback,
    bindingComponent,
    appExecutors,
    object : DiffUtil.ItemCallback<Repository>() {
        override fun areItemsTheSame(oldItem: Repository, newItem: Repository) =
            oldItem.name == newItem.name && oldItem.owner.login == newItem.owner.login

        override fun areContentsTheSame(oldItem: Repository, newItem: Repository) = oldItem == newItem
    }
) {
    override fun initBinding(binding: ViewDataBinding): ViewDataBinding {
        if (binding is ItemRepositoryBinding)
            binding.apply { clickCallback = itemClickCallback }

        return binding
    }

    override fun getViewType(position: Int) = R.layout.item_repository

    override fun bind(binding: ViewDataBinding, position: Int) {
        val outItem = getItem(position)
        if (binding is ItemRepositoryBinding) binding.item = outItem
    }
}