package com.pawthunder.githubexample.ui.repository

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.pawthunder.githubexample.R
import com.pawthunder.githubexample.databinding.FragmentRepositoryBinding
import com.pawthunder.githubexample.di.Injectable
import com.pawthunder.githubexample.util.autoCleared
import javax.inject.Inject

class RepositoryFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var repositoryModel: RepositoryViewModel

    private var binding by autoCleared<FragmentRepositoryBinding>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_repository, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        repositoryModel = ViewModelProviders.of(this, viewModelFactory).get(RepositoryViewModel::class.java)
    }
}
