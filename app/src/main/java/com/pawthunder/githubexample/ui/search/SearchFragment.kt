package com.pawthunder.githubexample.ui.search

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.pawthunder.githubexample.R
import com.pawthunder.githubexample.binding.FragmentDataBindingComponent
import com.pawthunder.githubexample.databinding.FragmentSearchBinding
import com.pawthunder.githubexample.di.Injectable
import com.pawthunder.githubexample.ui.common.ItemClickCallback
import com.pawthunder.githubexample.ui.repository.RepositotyAdapter
import com.pawthunder.githubexample.util.AppExecutors
import com.pawthunder.githubexample.util.autoCleared
import com.pawthunder.githubexample.vo.db.Repository
import javax.inject.Inject

/**
 * Fragment with search input for repository searching
 * @author Peter Grajko
 * @since 1.0
 */
class SearchFragment : Fragment(), Injectable, ItemClickCallback {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    private lateinit var searchModel: SearchViewModel

    private var binding by autoCleared<FragmentSearchBinding>()

    private var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        searchModel = ViewModelProviders.of(this, viewModelFactory).get(SearchViewModel::class.java)

        initList()
        initSearch()
    }

    override fun onItemClick(view: View) {
        if (view.tag is Repository) {
            val url = (view.tag as Repository).htmlUrl
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            activity?.startActivity(intent)
        }
    }

    private fun initList() {
        binding.searchRepoList.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = RepositotyAdapter(
                { searchModel.retry() },
                dataBindingComponent,
                appExecutors,
                this@SearchFragment
            ).apply { submitList(searchModel.repositoryList.value) }
        }

        searchModel.repositoryList.observe(this, Observer { items ->
            (binding.searchRepoList.adapter as RepositotyAdapter).submitList(items)
        })
    }

    private fun initSearch() {
        binding.searchInput.setOnEditorActionListener { view: View, actionId: Int, _: KeyEvent? ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                doSearch(view)
                true
            } else false
        }

        searchModel.networkState.observe(this, Observer { state ->
            val adapter = binding.searchRepoList.adapter as RepositotyAdapter
            adapter.setNetworkState(state)
        })
    }

    private fun doSearch(view: View) {
        (activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager)
            ?.hideSoftInputFromWindow(view.windowToken, 0)

        searchModel.showQuery(binding.searchInput.text.toString())
    }
}
