package com.pawthunder.githubexample.ui.common

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pawthunder.githubexample.R
import com.pawthunder.githubexample.vo.api.NetworkState
import com.pawthunder.githubexample.vo.api.Status
import kotlinx.android.synthetic.main.network_state_item.view.*

/**
 * Holder for network state in adapter. It shows during additional items loading
 * @author Peter Grajko
 * @since 1.0
 * @param retryCallback callback to reload items
 */
class NetworkStateItemViewHolder(
    view: View,
    private val retryCallback: () -> Unit
) : RecyclerView.ViewHolder(view) {

    private val progressBar = view.networkProgressBar
    private val retry = view.networkRetryButton
    private val errorMsg = view.networkErrorMessage

    init {
        retry.setOnClickListener { retryCallback() }
    }

    /**
     * Item containing progressBar, retry button, and error message which shows when needed
     * @param networkState load state determines visibility of views
     */
    fun bindTo(networkState: NetworkState?) {
        progressBar.visibility = if (networkState?.status == Status.LOADING) View.VISIBLE else View.GONE
        retry.visibility = if (networkState?.status == Status.ERROR) View.VISIBLE else View.GONE
        errorMsg.visibility = if (networkState?.message != null) View.VISIBLE else View.GONE
        errorMsg.text = networkState?.message
    }

    companion object {
        fun create(parent: ViewGroup, retryCallback: () -> Unit): NetworkStateItemViewHolder {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.network_state_item, parent, false)
            return NetworkStateItemViewHolder(view, retryCallback)
        }
    }
}