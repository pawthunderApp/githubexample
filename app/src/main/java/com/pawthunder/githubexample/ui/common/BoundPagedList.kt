package com.pawthunder.githubexample.ui.common

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.pawthunder.githubexample.R
import com.pawthunder.githubexample.util.AppExecutors
import com.pawthunder.githubexample.vo.api.NetworkState

/**
 * Multi type paged adapter used with pagination list
 * @author Peter Grajko
 * @since 1.0
 * @property retryCallback callback to try reload items
 * @property bindingComponent used during data binding
 */
abstract class BoundPagedList<T>(
    private val retryCallback: () -> Unit,
    private val bindingComponent: DataBindingComponent,
    appExecutors: AppExecutors,
    diffCallback: DiffUtil.ItemCallback<T>
) : PagedListAdapter<T, RecyclerView.ViewHolder>(
    AsyncDifferConfig.Builder<T>(diffCallback)
        .setBackgroundThreadExecutor(appExecutors.diskIO())
        .build()
) {

    private var networkState: NetworkState? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        if (viewType == R.layout.network_state_item) NetworkStateItemViewHolder.create(parent, retryCallback)
        else DataBoundHolder(
            initBinding(
                DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    viewType,
                    parent,
                    false,
                    bindingComponent
                )
            )
        )

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is NetworkStateItemViewHolder -> holder.bindTo(networkState)
            is DataBoundHolder<ViewDataBinding> -> {
                bind(holder.binding, position)
                holder.binding.executePendingBindings()
            }
        }
    }

    override fun getItemViewType(position: Int) =
        if (hasExtraRow() && position == itemCount - 1) {
            R.layout.network_state_item
        } else {
            getViewType(position)
        }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasExtraRow()) 1 else 0
    }

    /**
     * Network state is changed and need to be appropriately show to user as indication that loading is triggered.
     * It shows to user when more items needs to be loaded.
     * @param newNetworkState new state of pagination network handling
     */
    fun setNetworkState(newNetworkState: NetworkState?) {
        val previousState = this.networkState
        val hadExtraRow = hasExtraRow()
        this.networkState = newNetworkState
        val hasExtraRow = hasExtraRow()
        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {
                notifyItemRemoved(super.getItemCount())
            } else {
                notifyItemInserted(super.getItemCount())
            }
        } else if (hasExtraRow && previousState != newNetworkState) {
            notifyItemChanged(itemCount - 1)
        }
    }

    /**
     * Additional initialization of [ViewDataBinding] instance right after creation
     * @param binding for further initialization
     * @return Initialized data binding
     */
    abstract fun initBinding(binding: ViewDataBinding): ViewDataBinding

    /**
     * Layout type for item is requested
     * @param position of item which needs view type
     * @return layout type from resources
     */
    abstract fun getViewType(position: Int): Int

    /**
     * Item is bound to container
     * @param binding of container
     * @param item which is bound
     */
    abstract fun bind(binding: ViewDataBinding, position: Int)

    /**
     * Check if current networkState can have extra row
     * @return boolean if extra loading row can be added or removed
     */
    private fun hasExtraRow() = networkState != null && networkState != NetworkState.LOADED
}