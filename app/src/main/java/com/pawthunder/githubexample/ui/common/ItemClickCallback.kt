package com.pawthunder.githubexample.ui.common

import android.view.View

interface ItemClickCallback {

    fun onItemClick(view: View)
}