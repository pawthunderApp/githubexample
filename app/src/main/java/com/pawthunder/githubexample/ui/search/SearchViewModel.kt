package com.pawthunder.githubexample.ui.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.pawthunder.githubexample.repository.RepoRepository
import javax.inject.Inject

class SearchViewModel @Inject constructor(repoRepository: RepoRepository) : ViewModel() {

    private val searchQuery = MutableLiveData<String>()

    private val repositoryResult = Transformations.map(searchQuery) {
        repoRepository.searchRepositories(searchQuery.value ?: "")
    }

    val repositoryList = Transformations.switchMap(repositoryResult) { it.pagedList }
    val networkState = Transformations.switchMap(repositoryResult) { it.networkState }
    val refreshState = Transformations.switchMap(repositoryResult) { it.refreshState }

    /**
     * New search query was requested
     * @param query containing keyword for request
     * @return if new query was requested or not
     */
    fun showQuery(query: String): Boolean {
        if (searchQuery.value == query) {
            return false
        }

        searchQuery.value = query
        return true
    }

    fun refresh() {
        repositoryResult.value?.refresh?.invoke()
    }

    fun retry() {
        repositoryResult.value?.retry?.invoke()
    }

}