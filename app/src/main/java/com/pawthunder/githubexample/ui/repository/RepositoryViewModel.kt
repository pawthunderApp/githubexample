package com.pawthunder.githubexample.ui.repository

import androidx.lifecycle.ViewModel
import javax.inject.Inject

class RepositoryViewModel @Inject constructor() : ViewModel()