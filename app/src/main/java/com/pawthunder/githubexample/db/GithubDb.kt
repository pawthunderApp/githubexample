package com.pawthunder.githubexample.db


import androidx.room.Database
import androidx.room.RoomDatabase
import com.pawthunder.githubexample.vo.db.Repository
import com.pawthunder.githubexample.vo.db.User

/**
 * Main database description.
 */
@Database(
    entities = [Repository::class, User::class],
    version = 1,
    exportSchema = false
)
abstract class GithubDb : RoomDatabase() {

    abstract fun getRepositoryDao(): RepositoryDao

    abstract fun getUserDao(): UserDao
}
