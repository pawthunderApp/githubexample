package com.pawthunder.githubexample.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.pawthunder.githubexample.vo.db.User

@Dao
interface UserDao : DatabaseOperationDao<User> {

    @Query("SELECT * FROM User WHERE login = :login")
    fun findByLogin(login: String): LiveData<User>
}