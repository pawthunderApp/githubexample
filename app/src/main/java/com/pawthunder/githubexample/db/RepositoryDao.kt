package com.pawthunder.githubexample.db

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Query
import com.pawthunder.githubexample.vo.db.Repository

@Dao
interface RepositoryDao : DatabaseOperationDao<Repository> {

    @Query("SELECT * FROM Repository")
    fun loadAll(): DataSource.Factory<Int, Repository>

    @Query("SELECT * FROM Repository WHERE owner_login = :owner AND name = :name")
    fun load(owner: String, name: String): DataSource.Factory<Int, Repository>

    @Query("SELECT * FROM Repository WHERE owner_login = :owner ORDER BY name")
    fun loadByOwner(owner: String): DataSource.Factory<Int, Repository>

    @Query("SELECT * FROM Repository WHERE id IN (:repoIds)")
    fun loadById(repoIds: List<Int>): DataSource.Factory<Int, Repository>

    @Query(
        """
        SELECT * FROM Repository
        WHERE name LIKE :query OR fullName LIKE :query OR owner_login LIKE :query
        """
    )
    fun findAll(query: String): DataSource.Factory<Int, Repository>

}