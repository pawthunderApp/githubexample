package com.pawthunder.githubexample.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
