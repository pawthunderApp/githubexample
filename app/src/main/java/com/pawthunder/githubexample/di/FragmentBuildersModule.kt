package com.pawthunder.githubexample.di

import com.pawthunder.githubexample.ui.repository.RepositoryFragment
import com.pawthunder.githubexample.ui.search.SearchFragment
import com.pawthunder.githubexample.ui.user.UserFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeRepositoryFragment(): RepositoryFragment

    @ContributesAndroidInjector
    abstract fun contributeUserFragment(): UserFragment

    @ContributesAndroidInjector
    abstract fun contributeSearchFragment(): SearchFragment
}