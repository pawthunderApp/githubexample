package com.pawthunder.githubexample.di

import android.app.Application
import androidx.room.Room
import com.pawthunder.githubexample.AppConfig
import com.pawthunder.githubexample.api.GithubService
import com.pawthunder.githubexample.db.GithubDb
import com.pawthunder.githubexample.util.LiveDataCallAdapterFactory
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideGithubService() =
        Retrofit.Builder()
            .baseUrl(AppConfig.HOST)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .build()
            .create(GithubService::class.java)

    @Singleton
    @Provides
    fun provideDb(app: Application) =
        Room.databaseBuilder(app, GithubDb::class.java, "github.db")
            .addMigrations()
            .fallbackToDestructiveMigration()
            .build()

    @Singleton
    @Provides
    fun provideRepositoryDao(database: GithubDb) = database.getRepositoryDao()

    @Singleton
    @Provides
    fun provideUserDao(database: GithubDb) = database.getUserDao()

}