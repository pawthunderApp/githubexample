package com.pawthunder.githubexample.di

import dagger.Module

@Suppress("unused")
@Module
abstract class ServiceModule