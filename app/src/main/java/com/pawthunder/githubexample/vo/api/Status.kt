package com.pawthunder.githubexample.vo.api

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}