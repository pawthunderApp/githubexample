package com.pawthunder.githubexample.vo.db

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Index
import com.google.gson.annotations.SerializedName
import timber.log.Timber
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

@Entity(
    indices = [
        Index("id"),
        Index("owner_login")],
    primaryKeys = ["name", "owner_login"]
)
data class Repository(
    val id: Int,
    val name: String,
    @SerializedName("full_name")
    val fullName: String,
    val description: String?,
    @SerializedName("html_url")
    val htmlUrl: String?,
    @field:SerializedName("owner")
    @field:Embedded(prefix = "owner_")
    val owner: Owner,
    val language: String?,
    @SerializedName("updated_at")
    val updatedAt: String?,
    val createdAt: String?,
    val score: Double
){


    data class Owner(
        val login: String,
        val url: String?,
        @SerializedName("avatar_url")
        val avatarUrl: String?

    )


    /**
     * Date is formed for output
     * @param timestamp formed to output date
     * @return output date
     */
    fun formatDate(timestamp: String?): String {
        if (timestamp == null) return ""
        val formatOut = SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.US)
        val formatIn = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US)

        try {
            return formatOut.format(formatIn.parse(timestamp))
        } catch (ex: ParseException) {
            Timber.e("Problem parsing date format")
        }
        return ""
    }
}