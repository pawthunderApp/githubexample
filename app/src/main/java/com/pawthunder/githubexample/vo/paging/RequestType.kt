package com.pawthunder.githubexample.vo.paging

enum class RequestType {
    INITIAL,
    BEFORE,
    AFTER
}