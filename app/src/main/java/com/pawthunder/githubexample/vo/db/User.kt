package com.pawthunder.githubexample.vo.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class User(
    @PrimaryKey
    val login: String,
    @field:SerializedName("avatar_url")
    val avatarUrl: String?,
    val name: String?,
    val company: String?,
    @SerializedName("repos_url")
    val reposUrl: String?,
    val blog: String?
)