package com.pawthunder.githubexample.vo.paging

import com.pawthunder.githubexample.vo.api.Status

data class StatusReport(
    val initial: Status,
    val before: Status,
    val after: Status,
    private val mErrors: Array<Throwable?>
) {

    /**
     * Convenience method to check if there are any running requests.
     *
     * @return True if there are any running requests, false otherwise.
     */
    fun hasRunning(): Boolean {
        return (initial === Status.LOADING
                || before === Status.LOADING
                || after === Status.LOADING)
    }

    /**
     * Convenience method to check if there are any requests that resulted in an error.
     *
     * @return True if there are any requests that finished with error, false otherwise.
     */
    fun hasError(): Boolean {
        return (initial === Status.ERROR
                || before === Status.ERROR
                || after === Status.ERROR)
    }

    /**
     * Returns the error for the given request type.
     *
     * @param type The request type for which the error should be returned.
     * @return The [Throwable] returned by the failing request with the given type or
     * `null` if the request for the given type did not fail.
     */
    fun getErrorFor(type: RequestType): Throwable? {
        return mErrors[type.ordinal]
    }
}