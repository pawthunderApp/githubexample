package com.pawthunder.githubexample.vo.response

import com.google.gson.annotations.SerializedName
import com.pawthunder.githubexample.vo.db.Repository

data class RepositorySearchResponse(
    @SerializedName("total_count")
    val totalCount: Int = 0,
    val items: List<Repository>
)